#
# Cookbook:: spring-boot-application
# Recipe:: default
#
# Copyright:: 2018, Harrison Kim, All Rights Reserved.

include_recipe 'spring-boot-application::install_packages'
include_recipe 'spring-boot-application::download_artifacts'
include_recipe 'spring-boot-application::deploy_application'
