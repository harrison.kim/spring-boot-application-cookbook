#
# Cookbook:: spring-boot-application-cookbook
# Recipe:: install_packages
#
# Copyright:: 2018, Harrison Kim, All Rights Reserved.

puts 'Installing packages!'

include_recipe 'poise-python'

package 'java' do
  action :install
end

package 'tree' do
  action :install
end
